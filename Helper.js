function Helper (){
}

Helper.prototype.getNewtonURL = function(operation, expression) { //Gets the url to call for the calculation
    return `https://newton.now.sh/${operation}/${encodeURIComponent(expression)}`;
}

Helper.prototype.validate = function(result){
    if (result == 0) return "zilch";
    if (result && typeof result==="string" && result != 'undefined') { //Provides the safe result to the bot
        return {
            fulfillmentText: result
        }
    } else {
        return { //Fails Gracefully for the User
            fulfillmentText: "Apologies, I seem to have a bit of a brain freeze"
        }
    }
}

module.exports = Helper
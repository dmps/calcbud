# CalcBud - Your Math Genius Buddy

## Goals

- Create Chatbot UI on Dialogflow (done)

- Create Backend API to call to Newton (done)

- Return human readable response to the user (done)

- Test out my new algebraic calculator (done)

## Stretch Goals

- Be able to use variables other than x for differentiation

- Give the bot some personality, (really shy?)

## URL: https://console.dialogflow.com/api-client/demo/embedded/031fc7f6-25d0-4d38-aa82-c90d1ce65278
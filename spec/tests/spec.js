describe("Helper functions", function () {
  const Helper = require('../../Helper')
  beforeEach(function () {
    helper = new Helper();
  });

  it('Helper should exist', () => {
    expect(helper).toBeDefined('Helper module does not load')
  });

  it('Helper.getNewtonURL should return correct URL', () => {
    expect(helper.getNewtonURL('factor', 'x^2-4')).toBe('https://newton.now.sh/factor/x%5E2-4', 'Helper.getNewtonURL returns false result');
  })

  it('Helper.validate should return a valid response object', () => {
    //check errors
    expect(helper.validate('undefined').fulfillmentText).toBe('Apologies, I seem to have a bit of a brain freeze', 'Helper.validate cannot handle undefined');

    //Check zeros
    expect(helper.validate('0')).toBe('zilch', 'Helper.validate cannot handle 0 (str)');
    expect(helper.validate(0)).toBe('zilch', 'Helper.validate cannot handle 0 (num)');

    //Check correct input
    expect(helper.validate('x^2-4').fulfillmentText).toBe('x^2-4', 'Helper.validate cannot handle correct input');
  })
});
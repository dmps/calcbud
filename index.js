const request = require('request');
const Helper = require('./Helper.js');

module.exports = function (ctxt, done) {
    request.get(Helper.getNewtonURL(ctxt.body.queryResult.parameters.operation, ctxt.body.queryResult.parameters.expression), (err, res, body) => {
        if (err) throw err;
        done(null, Helper.validate(JSON.parse(body).result));
    });
};